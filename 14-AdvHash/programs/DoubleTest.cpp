#include "../classes/LinearProbe.h"
#include "../classes/DoubleHash.h"

void printStatus(const HashTable<int, int>&  linearHash,
				 const DoubleHash<int, int>& doubleHash ){
	
	std::cout << "Linear Hash Results: " << std::endl;
	std::cout << linearHash << std::endl << std::endl;
	
	std::cout << "Double Hash Results: " << std::endl;
	std::cout << doubleHash << std::endl << std::endl;
	
}

int main(){
	
	HashTable<int, int> linearHash(7);
	DoubleHash<int, int> doubleHash(7);
	
	linearHash.insert(76);	doubleHash.insert(76);
	linearHash.insert(40);	doubleHash.insert(40);
	linearHash.insert(48);	doubleHash.insert(48);
	
	printStatus(linearHash, doubleHash);
	
	linearHash.insert(5);	doubleHash.insert(5);
	linearHash.insert(55);	doubleHash.insert(55);

	printStatus(linearHash, doubleHash);

	linearHash.insert(10);	doubleHash.insert(10);
	linearHash.insert(18);	doubleHash.insert(18);
	
	printStatus(linearHash, doubleHash);
	
	linearHash.insert(44);	doubleHash.insert(44);
	linearHash.insert(96);	doubleHash.insert(96);
	
	printStatus(linearHash, doubleHash);
	
	return 0;
}