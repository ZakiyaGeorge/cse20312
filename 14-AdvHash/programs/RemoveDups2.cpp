/**********************************************
* File: RemoveDups.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This is the main driver program for Problem 2
* in Lecture 12 
**********************************************/

#include <vector>
#include <unordered_map>
#include <iostream>

int main(){

	std::string theString("Ghjfhfjghsjdgjldjlgajgajgflajlablgjhkhffhhakgkagfjagfjhiiiwyhaf,bz");
	std::unordered_map<char, bool> theHash;

	std::cout << "Removing duplicates from " << theString;
	
	unsigned int str_iter = 0;
	while(str_iter < theString.size()){
		
		// If not found, Hash and then increment str_iter by 1
		if( theHash.count( theString.at(str_iter) ) == 0 ){
			theHash.insert( {theString.at(str_iter) , true} );
			str_iter++;
		}
		else{
			// Remove the duplicate char and do NOT increment iterator
			theString.erase( theString.begin() + str_iter );
		}
	}
	
	std::cout << " gives " << theString << std::endl;

}
