/**********************************************
* File: Check.h
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Class Declaration for Check 
**********************************************/

#ifndef CHECK_H
#define CHECK_H

#include <string>
#include <iostream>
#include "Money.h"

class Check{


	private:
		unsigned int checkNum;
		std::string date;
		std::string payer;
		std::string payee;
		Money value;


	public:
	
		Check(unsigned int checkNumIn, std::string dateIn, std::string payerIn,
			std::string payeeIn, Money valueIn);
			
			
		Money getValue();
		
		bool isPayer();
		
		bool isPayee();
			
		friend std::ostream& operator<<(std::ostream& out, const Check& print);
			

};

#endif
