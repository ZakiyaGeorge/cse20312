/**********************************************
* File: Check.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Class method definitions for Check 
**********************************************/

#include "Check.h"

Check::Check(unsigned int checkNumIn, std::string dateIn, std::string payerIn,
	std::string payeeIn, Money valueIn) :
	checkNum(checkNumIn), date(dateIn), payer(payerIn), 
	payee(payeeIn), value(valueIn) {}



Money Check::getValue(){
	return value;
}


bool Check::isPayer(){
	return payer == "SELF";
}

bool Check::isPayee(){
	return payee == "SELF";
}
		

std::ostream& operator<<(std::ostream& out, const Check& print){
	
	out << print.checkNum << ": " << std::endl;
	out << "Date: " << print.date << ". ";
	out << "Payer: " << print.payer << ", ";
	out << "Payee: " << print.payee << ", ";	
	out << "Value: " << print.value << std::endl;	
	
	return out;
}