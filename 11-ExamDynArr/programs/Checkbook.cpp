/**********************************************
* File: Checkbook.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Main driver program for Exam Example 1
**********************************************/

#include "../classes/Check.h"
#include "../classes/Money.h"
#include <vector>

int main(){

	// Create a vector of Checks
	std::vector< Check > Checkbook;
	
	// Create a Money for Balance for $1842.99
	Money balance(1842, 99);
	
	// Create five checks
	Check check1(1071, "02/21/2019", "Work", "SELF", Money(1042, 83));
	Check check2(1072, "02/27/2019", "SELF", "Electric", Money(207, 33));
	Check check3(1073, "03/01/2019", "SELF", "Light", Money(44, 9));
	Check check4(1074, "03/15/2019", "Work", "SELF", Money(1000, 82));
	Check check5(1075, "03/16/2019", "SELF", "Grocery", Money(101, 47));
	Check check6(1076, "03/19/2019", "Work", "Client", Money(101, 47));
	
	// Push back the checks onto the vector
	Checkbook.push_back(check1);
	Checkbook.push_back(check2);
	Checkbook.push_back(check3);
	Checkbook.push_back(check4);
	Checkbook.push_back(check5);
	Checkbook.push_back(check6);
	
	// Add or subtract to balance, and then print
	for(Check& theCheck : Checkbook){

		std::cout << theCheck;
		
		if(theCheck.isPayer()){
			balance = balance - theCheck.getValue();
		}
		else if(theCheck.isPayee()){
			balance = balance + theCheck.getValue();
		}
		else{
			std::cout << "Not Payer or Payee! Return to Payer!" << std::endl;
		}
		
		std::cout << "Balance after transaction: " << balance << std::endl;
	}

	return 0;
}
