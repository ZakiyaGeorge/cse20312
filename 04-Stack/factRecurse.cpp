/**********************************************
* File: factRecurse.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This contains an example of recursion used 
* in Lecture 4 to show how the stack works
**********************************************/
#include <iostream> /* For std::cout */

/********************************************
* Function Name  : factorial
* Pre-conditions : int num
* Post-conditions: int
* 
* This function calculate the factorial of num
* and returns an int. 
********************************************/
int factorial(int num){
	
	/* Base Case */
	if(num == 0){
		return 1;
	}
	
	/* Recursive Case */
	else{
		return num * factorial(num - 1);
	}

}


/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){
	
	/* Declare variables */
	int num = 3;
	std::cout << num << "! = " << factorial(num) << "\n";

	return 0;
}
